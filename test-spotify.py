import spotipy
from spotipy import util

token = util.prompt_for_user_token(
    "nils.holle",
    scope="user-library-read",
    client_id="cd884a0befa14d15ac5dc110154a4dd8",
    client_secret="5c5cde4129a54dd7aacca82f8f7ece79",
    redirect_uri="http://example.com/callback/"
)

birdy_uri = 'spotify:artist:2WX2uTcsvV5OnS0inACecP'
spotify = spotipy.Spotify(auth=token)

results = spotify.artist_albums(birdy_uri, album_type='album')
albums = results['items']
while results['next']:
    results = spotify.next(results)
    albums.extend(results['items'])

for album in albums:
    print(album['name'])
