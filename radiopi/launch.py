import os

from radiopi.ui.main import Main


def main():
    """ Create Application and MainWindow. """
    Main().run()

if __name__ == "__main__":
    sys.exit(main())
