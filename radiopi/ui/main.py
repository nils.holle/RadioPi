import locale
import os
from datetime import datetime
import eyed3

from kivy.adapters.listadapter import ListAdapter
from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.lang import Builder
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.listview import ListItemButton, ListView
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.widget import Widget
from weather import Unit, Weather

from radiopi.backend import vlc
from radiopi.backend.radiostations import RadioStations
from radiopi.backend.signal import Signal


class MainWidget(Widget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__currentPlayer = None
        self.__stations = RadioStations()
        self.__stations.load_playlist()

        self.radioView = self.ids["radio_view"]
        self.radioView.setStations(self.__stations.data)
        self.radioView.selectionChanged.connect(self.__radioSelectionChanged)
        self.lblVolume = self.ids["lblVolume"]
        self.lblPlaying = self.ids["lblPlaying"]
        self.currentVolume = 25
        self.__muteVolume = 25

        self.lblClock = self.ids["lblClock"]
        Clock.schedule_interval(self.updateClock, 1)

        Clock.schedule_once(self.updateWeather, 1)
        Clock.schedule_interval(self.updateWeather, 300)

    @property
    def currentVolume(self):
        return self.__currentVolume

    @currentVolume.setter
    def currentVolume(self, volume: int):
        if volume >= 0 and volume <= 100:
            self.__currentVolume = volume
        self.lblVolume.text = str(self.__currentVolume)
        if self.__currentPlayer is not None:
            self.__currentPlayer.audio_set_volume(self.__currentVolume)

    def updateWeather(self, *args):
        try:
            label = self.ids["lblWeather"]
            weather = Weather(unit=Unit.CELSIUS)
            lookup = weather.lookup_by_location("aachen")
        except Exception as e:
            print(e)
            return

        label.text = lookup.condition.text + ", " + \
            str(lookup.condition.temp) + "°C\n\n"

        label = self.ids["lblForecast"]
        label.text = ""
        for f in lookup.forecast:
            date = datetime.strptime(f.date, "%d %b %Y")
            label.text += date.strftime("%A, %x") + ": " + f.text + \
                "; min. " + str(f.low) + "°C, max. " + str(f.high) + \
                "°C" + "\n"

    def __radioSelectionChanged(self, text: str):
        if self.__currentPlayer is not None:
            self.__currentPlayer.stop()
            self.__currentPlayer = None

        self.__currentPlayer = vlc.MediaPlayer(
            self.__stations.getStreamURLbyName(text))
        self.lblPlaying.text = text
        self.__currentPlayer.play()
        self.__currentPlayer.audio_set_volume(self.currentVolume)

    def playFile(self, filePath: str):
        print(filePath)
        if self.__currentPlayer is not None:
            self.__currentPlayer.stop()
            self.__currentPlayer = None

        self.__currentPlayer = vlc.MediaPlayer(filePath)
        if filePath.endswith(".mp3"):
            tag = eyed3.load(filePath).tag
            self.lblPlaying.text = "%s\n%s\n%s" % (
                tag.title, tag.artist, tag.album)
        self.__currentPlayer.play()
        self.__currentPlayer.audio_set_volume(self.currentVolume)

    def playFiles(self, *args):
        if len(args) > 1:
            if isinstance(args[1], list):
                if len(args[1]) > 0:
                    self.playFile(args[1][0])

    def startPlayback(self):
        if self.__currentPlayer is not None:
            self.__currentPlayer.play()

    def pausePlayback(self):
        if self.__currentPlayer is not None:
            self.__currentPlayer.stop()

    def stopPlayback(self):
        if self.__currentPlayer is not None:
            self.__currentPlayer.stop()
            self.lblPlaying.text = ""
            self.radioView.clearSelection()

    def volumeUp(self, *args):
        self.currentVolume += 1

    def volumeDown(self, *args):
        self.currentVolume -= 1

    def _startVolumeUp(self):
        self.volumeUp()
        Clock.schedule_interval(self.volumeUp, 0.1)

    def _stopVolumeUp(self):
        Clock.unschedule(self.volumeUp)

    def _startVolumeDown(self):
        self.volumeDown()
        Clock.schedule_interval(self.volumeDown, 0.1)

    def _stopVolumeDown(self):
        Clock.unschedule(self.volumeDown)

    def toggleMute(self):
        if self.currentVolume == 0:
            self.currentVolume = self.__muteVolume
        else:
            self.__muteVolume = self.currentVolume
            self.currentVolume = 0

    def updateClock(self, *args):
        locale.setlocale(locale.LC_ALL, "de_DE.utf8")
        d = datetime.now()
        text = d.strftime("%X\n%x")
        self.lblClock.text = text

    def shutdown(self):
        os.system("systemctl poweroff")

    def reboot(self):
        os.system("systemctl reboot")


class RadioListAdapter(ListAdapter):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__selectionChanged = Signal()

    @property
    def selectionChanged(self) -> Signal:
        return self.__selectionChanged

    @property
    def currentName(self) -> str:
        if len(super().selection) > 0:
            return super().selection[0].text
        return None

    def on_selection_change(self, *args):
        name = self.currentName
        if name is not None:
            self.selectionChanged.emit(name)
        super().on_selection_change(*args)


class RadioView(ListView):

    def __init__(self, **kwargs):
        def converter(row_index, rec):
            return {"text": rec["text"], "size_hint_y": None, "height": 50}

        super().__init__(
            **kwargs,
            adapter=RadioListAdapter(
                data=[],
                args_converter=converter,
                cls=ListItemButton,
                selection_mode="single"
            ))

        self.__pos = 0

    @property
    def selectionChanged(self):
        return self.adapter.selectionChanged

    def setStations(self, stations: list):
        self.adapter.data = [{"text": s["name"], "is_selected": False}
                             for s in stations]

    def clearSelection(self):
        self.adapter.selection_mode = "none"
        self.adapter.selection_mode = "single"

    def scrollUp(self, *args):
        if self.__pos > 0:
            self.__pos -= 1
            self.scroll_to(self.__pos)

    def _startScrollUp(self):
        self.scrollUp()
        Clock.schedule_interval(self.scrollUp, 0.1)

    def _stopScrollUp(self):
        Clock.unschedule(self.scrollUp)

    def scrollDown(self, *args):
        if self.__pos < len(self.adapter.data):
            self.__pos += 1
            self.scroll_to(self.__pos)

    def _startScrollDown(self):
        self.scrollDown()
        Clock.schedule_interval(self.scrollDown, 0.1)

    def _stopScrollDown(self):
        Clock.unschedule(self.scrollDown)


class RadioFileChooser(FileChooserIconView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__pos = 0

    def scrollUp(self, *args):
        scr = self.layout.ids["scrollview"]
        if scr.scroll_y < 1:
            scr.scroll_y += 1 / len(self.files)

    def _startScrollUp(self):
        self.scrollUp()
        Clock.schedule_interval(self.scrollUp, 0.1)

    def _stopScrollUp(self):
        Clock.unschedule(self.scrollUp)

    def scrollDown(self, *args):
        scr = self.layout.ids["scrollview"]
        if scr.scroll_y > 0:
            scr.scroll_y -= 1 / len(self.files)

    def _startScrollDown(self):
        self.scrollDown()
        Clock.schedule_interval(self.scrollDown, 0.1)

    def _stopScrollDown(self):
        Clock.unschedule(self.scrollDown)

    def play(self, files: list):
        f = files[0]


class Main(App):

    def build(self):
        Config.set('graphics', 'width', '800')
        Config.set('graphics', 'height', '480')
        return MainWidget()


if __name__ == "__main__":
    Main().run()
